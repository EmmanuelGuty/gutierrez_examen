package project_java;

import java.util.ArrayList;
import java.util.Scanner;

public class Project_java {
   
   static int bestYet = 0;
   static ArrayList<int[]> L = new ArrayList<int[]>();
      
   public static void main(String [] args) {
      System.out.println("ISAAC OMAR REYES LARA");
      String line;
      int ncases;
      Scanner in = new Scanner(System.in);

      int n = in.nextInt();
      in.nextLine();
       
      int casenum = 1;
      while(n > 0) {
      	 int[][] A = new int[n][3];
      	 for(int i=0; i<n; i++) {
      	    int a = in.nextInt();
      	    int b = in.nextInt();
      	    int c = in.nextInt();
      	    int t = 0;
      	    if (b < a) {
      	       t = a;
      	       a = b;
      	       b = t;
      	    }
      	    if (c < b) {
      	       t = b;
      	       b = c;
      	       c = t;
      	    }
      	    if (b < a) {
      	       t = a;
      	       a = b;
      	       b = t;
      	    }
      	    A[i][0] = a;
      	    A[i][1] = b;
      	    A[i][2] = c;
      	    in.nextLine();
      	 } 
      	 bestYet = 0;
      	 R(A, 0);   
      	 System.out.println("Case " + casenum+ ": " + bestYet);
      	 casenum++;
      	 n = in.nextInt();	
      }
      
   }
   public static void R(int[][] A, int i) {
      if (L.size() > bestYet) bestYet = L.size();
      if (i < A.length) {
      	 if (Try(A[i][0], A[i][1])) {
      	    R(A, i+1);
      	    Remove(A[i][0], A[i][1]);
      	 }
      	 if (Try(A[i][0], A[i][2])) {
      	    R(A, i+1);
      	    Remove(A[i][0], A[i][2]);
      	 }
      	 if (Try(A[i][1], A[i][2])) {
      	    R(A, i+1);
      	    Remove(A[i][1], A[i][2]);
      	 }
      	 R(A,i+1);
      }
   }
   
   public static boolean Try(int a, int b) {
      int l = L.size();
      int[] p = {a,b};
      if (l == 0) {
      	 L.add(p);
      	 return true;
      }
      if(a <= L.get(0)[0] && b <= L.get(0)[1]) {
      	 L.add(0, p);
      	 return true;
      }
      if(a >= L.get(l-1)[0] && b >= L.get(l-1)[1]) {	    
      	 L.add(p);
      	 return true;
      }
      for(int i = 0; i < l-1; i++) {
      	 if(L.get(i)[0] <= a && a <= L.get(i+1)[0] && L.get(i)[1] <= b && b <= L.get(i+1)[1]) {
      	    L.add(i+1,p);
      	    return true;
      	 }
      }
      return false;
   }
   
   public static void Remove(int a, int b) {
      for(int i = 0; i < L.size(); i++) {
      	 if(L.get(i)[0] == a && L.get(i)[1] == b) {L.remove(i);
      	 return;}
      }
   }
}
